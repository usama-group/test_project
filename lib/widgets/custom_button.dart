import 'package:flutter/material.dart';
import 'package:task_project/base/device_util.dart';

import '../resources/resources.dart';

Widget customButton(
    {void Function()? onClick,
      String? text,
      Color? textColor,
      Color? bgColor,
      double? bottomLeftRadius,
      double? bottomRightRadius,
      double? topLeftRadius,
      double? topRightRadius}) {
  return InkWell(
    onTap: onClick,
    child: Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.blue),
          color: bgColor,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(bottomLeftRadius ?? 0),
              bottomRight: Radius.circular(bottomRightRadius ?? 0),
              topLeft: Radius.circular(topLeftRadius ?? 0),
              topRight: Radius.circular(topRightRadius ?? 0))),
      child: Padding(
        padding:  EdgeInsets.symmetric(vertical: 10, horizontal:DeviceUtil.isTablet? 50:15),
        child: Text(
          text!,
          style: R.textStyle.mediumLato().copyWith(color: textColor,),
        ),
      ),
    ),
  );
}