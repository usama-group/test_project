import 'package:flutter/material.dart';
import 'package:task_project/base/device_util.dart';

import '../base/resizer/fetch_pixels.dart';
import '../base/widget_utils.dart';
import '../resources/resources.dart';

class MyButton extends StatefulWidget {
  final Function onTap;
  final String buttonText;
  final bool isPrefixIcon;
  final Color? color;
  final Color? textColor;
  final double? height;
  final double? width;

  MyButton(
      {required this.onTap,
      required this.buttonText,
      this.textColor,
      this.isPrefixIcon = false,
      this.color,
      this.height,
      this.width});
  @override
  _MyButtonState createState() => _MyButtonState();
}

class _MyButtonState extends State<MyButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(35),
      onTap: () {
        widget.onTap();
      },
      child: Container(
        padding:
            EdgeInsets.symmetric(horizontal: FetchPixels.getPixelWidth(20)),
        height: widget.height ?? FetchPixels.getPixelHeight(60),
        width: widget.width?? FetchPixels.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(FetchPixels.getPixelHeight(DeviceUtil.isTablet?20:12)),
          // border: Border.all(color: R.colors.whiteColor,width: 1),
          gradient: LinearGradient(
            colors: [
              R.colors.buttonG1,
              R.colors.buttonG2,

            ],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
        ),
        child:   Center(
          child: Text(
            widget.buttonText,
            style: R.textStyle
                .mediumLato()
                .copyWith(color: R.colors.whiteColor,fontSize: FetchPixels.getPixelWidth(DeviceUtil.isTablet? 4:10)),
          ),
        ),
      ),
    );

    //   MaterialButton(
    //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15),
    //   side: BorderSide(color:R.colors.whiteColor,width: 2)
    //   ),
    //
    //   elevation: 0,
    //   minWidth: Get.width * 0.3,
    //   height: Get.height * .07,
    //   color: widget.color ?? R.colors.theme,
    //
    //   onPressed: () {
    //     widget.onTap();
    //   },
    // );
  }
}
