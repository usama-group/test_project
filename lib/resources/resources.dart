import 'package:task_project/resources/text_style.dart';

import '../base/constants/string_resource.dart';
import 'colors.dart';
import 'dummy_text.dart';
import 'images.dart';
class R {
  static AppColors colors = AppColors();
  static AppImages images = AppImages();
  static AppDummyData dummyData = AppDummyData();
  static Strings strings = Strings();
  static AppTextStyle textStyle = AppTextStyle();
}
