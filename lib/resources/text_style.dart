import 'package:flutter/cupertino.dart';

import '../resources/resources.dart';

class AppTextStyle {
  ///textstyles///

  TextStyle regularLato() {
    return TextStyle(
      // fontSize: 12,
      fontFamily: 'Lato',
      letterSpacing: 0.05,
      color: R.colors.whiteColor,
      fontWeight: FontWeight.w400,
    );
  }

  TextStyle semiBoldLato() {
    return TextStyle(
      // fontSize: 15,
      fontFamily: 'Lato',
      letterSpacing: 0.05,
      color: R.colors.whiteColor,
      fontWeight: FontWeight.w600,
    );
  }

  // TextStyle boldMetropolis() {
  //   return TextStyle(
  //     fontSize: 15,
  //     fontFamily: 'Metropolis',
  //     letterSpacing: 0,
  //     color: R.colors.whiteColor,
  //     fontWeight: FontWeight.w700,
  //   );
  // }


  TextStyle mediumLato() {
    return TextStyle(
      // fontSize: 15,
      fontFamily: 'Lato',
      letterSpacing: 0.05,
      color: R.colors.whiteColor,
      fontWeight: FontWeight.w500,
    );
  }
}
