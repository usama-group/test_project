import 'package:flutter/material.dart';

class AppColors {
  Color whiteColor = Colors.white;
  Color blackColor = Colors.black;
  Color transparent = Colors.transparent;
  Color theme = const Color(0xFFF26223);
  Color containerG1 = const Color(0xFFEBF4FF);
  Color containerG2 = const Color(0xFFE6FFFA);
  Color buttonG1 = const Color(0xFF319795);
  Color buttonG2 = const Color(0xFF3182CE);
  Color boldTextColor = const Color(0xFF2D3748);
  Color container2G1 = const Color(0xFFE6FFFA);
  Color container2G2 = const Color(0xFFEBF4FF);
  Color textColorGreen = const Color(0xFF319795);
  Color cirContainer = const Color(0xFFF7FAFC);
  Color extraBoldColor = const Color(0xFF718096);





}
