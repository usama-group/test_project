import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:task_project/screen/pages/landing_page_view.dart';
import 'package:task_project/screen/provider/landing_page_provider.dart';

import 'base/resizer/fetch_pixels.dart';

//Query Tickets
void main() {
  runApp(
      MultiProvider(providers: [
        ChangeNotifierProvider(create: (_) => LandingPageProvider()),
      ], child: const MyApp()));
}

//Stock Management

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FetchPixels(context);
    return  const MaterialApp(


      title: 'Test Project',
      debugShowCheckedModeBanner: false,
      home: LandingPageView(),
    );
  }
}
