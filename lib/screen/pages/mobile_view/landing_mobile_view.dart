import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../base/resizer/fetch_pixels.dart';
import '../../../base/widget_utils.dart';
import '../../../resources/resources.dart';
import '../../../widgets/custom_button.dart';
import '../../clippers/clippers_class.dart';
import '../../provider/landing_page_provider.dart';
class LandingMobileView extends StatelessWidget {
  const LandingMobileView({super.key});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ClipPath(
          clipper: WaveClipper(),
          child: Container(
            padding: EdgeInsets.symmetric(
                vertical: FetchPixels.getPixelHeight(30)),
            width: FetchPixels.width,
            // height: FetchPixels.getPixelHeight(350),
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
                  R.colors.containerG1,
                  R.colors.containerG2,
                ]),
                color: R.colors.blackColor),
            child: Column(
              children: [
                Text(
                  R.strings.Deinejob,
                  textAlign: TextAlign.center,
                  style: R.textStyle.semiBoldLato().copyWith(
                      fontSize: FetchPixels.getPixelWidth(22),
                      color: R.colors.boldTextColor),
                ),
                Container(
                  height: FetchPixels.getPixelHeight(580),
                  width: FetchPixels.width,
                  decoration: BoxDecoration(
                    // color: R.colors.whiteColor,

                      image: getDecorationAssetImage(
                          context, R.images.jobImage1,
                          fit: BoxFit.cover)),
                ),
              ],
            ),
          ),
        ),
        Container(
          // height: FetchPixels.getPixelHeight(350),
          decoration:
          BoxDecoration(color: R.colors.transparent),
          child: Column(
            children: [
              Consumer<LandingPageProvider>(
                builder: (context, provider, child) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      customButton(
                          onClick: () {
                            provider.toogleIndex = 0;
                            provider.update();
                          },
                          topLeftRadius: 15,
                          bottomLeftRadius: 15,
                          bgColor: provider.toogleIndex == 0
                              ? R.colors.textColorGreen
                              : Colors.white,
                          text: R.strings.arbeitnehmer,
                          textColor: provider.toogleIndex == 0
                              ? Colors.white
                              : R.colors.textColorGreen),
                      customButton(
                          onClick: () {
                            provider.toogleIndex = 1;
                            provider.update();
                          },
                          bgColor: provider.toogleIndex == 1
                              ? R.colors.textColorGreen
                              : Colors.white,
                          text: R.strings.arbeitgeber,
                          textColor: provider.toogleIndex == 1
                              ? Colors.white
                              : R.colors.textColorGreen),
                      customButton(
                          onClick: () {
                            provider.toogleIndex = 2;
                            provider.update();
                          },
                          topRightRadius: 15,
                          bottomRightRadius: 15,
                          bgColor: provider.toogleIndex == 2
                              ? R.colors.textColorGreen
                              : Colors.white,
                          text: R.strings.temporarburo,
                          textColor: provider.toogleIndex == 2
                              ? Colors.white
                              : R.colors.textColorGreen)
                    ],
                  );
                },
              ),
              getVerSpace(10),
              Text(
                R.strings.Dreieinfache,
                style: R.textStyle.semiBoldLato().copyWith(
                    fontSize: FetchPixels.getPixelWidth(18),
                    color: R.colors.boldTextColor),
              ),
              getVerSpace(10),
              SizedBox(
                // height: FetchPixels.getPixelHeight(150),
                width: FetchPixels.width,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Container(
                        margin: EdgeInsets.only(
                            top:
                            FetchPixels.getPixelHeight(100),
                            right:
                            FetchPixels.getPixelWidth(200)),
                        height: FetchPixels.getPixelHeight(250),
                        width: FetchPixels.getPixelWidth(150),
                        decoration: BoxDecoration(
                            color: R.colors.cirContainer,
                            shape: BoxShape.circle),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: FetchPixels.getPixelHeight(150),
                          left: FetchPixels.getPixelWidth(10)),
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.start,
                        crossAxisAlignment:
                        CrossAxisAlignment.end,
                        children: [
                          Text(
                            "1.",
                            style: R.textStyle
                                .semiBoldLato()
                                .copyWith(
                                fontSize: FetchPixels
                                    .getPixelWidth(80),
                                color: R
                                    .colors.extraBoldColor),
                          ),
                          getHorSpace(20),
                          Padding(
                            padding: EdgeInsets.only(
                                bottom:
                                FetchPixels.getPixelHeight(
                                    30)),
                            child: Text(
                              R.strings.ErstellendeinLebenslauf,
                              style: R.textStyle
                                  .semiBoldLato()
                                  .copyWith(
                                  fontSize: FetchPixels
                                      .getPixelWidth(12),
                                  color: R.colors
                                      .extraBoldColor),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.topCenter,
                      child: getAssetImage(R.images.jobImage2,
                          height:
                          FetchPixels.getPixelHeight(200),
                          width:
                          FetchPixels.getPixelWidth(150)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: FetchPixels.getPixelHeight(50)),
          // height: FetchPixels.getPixelHeight(400),
          decoration: BoxDecoration(
              image: getDecorationAssetImage(context, R.images.bgImage,fit: BoxFit.fill),
              color: R.colors.transparent),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    // top: FetchPixels.getPixelHeight(50),
                    left: FetchPixels.getPixelWidth(50)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      "2.",
                      style: R.textStyle
                          .semiBoldLato()
                          .copyWith(
                          fontSize:
                          FetchPixels.getPixelWidth(
                              80),
                          color: R.colors.extraBoldColor),
                    ),
                    getHorSpace(20),
                    Padding(
                      padding: EdgeInsets.only(
                          bottom:
                          FetchPixels.getPixelHeight(30)),
                      child: Text(
                        R.strings.ErstellendeinLebenslauf,
                        style: R.textStyle
                            .semiBoldLato()
                            .copyWith(
                            fontSize:
                            FetchPixels.getPixelWidth(
                                12),
                            color:
                            R.colors.extraBoldColor),
                      ),
                    ),
                  ],
                ),
              ),
              getAssetImage(R.images.jobImage3,
                  height: FetchPixels.getPixelHeight(150),
                  width: FetchPixels.getPixelWidth(150)),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(
              horizontal: FetchPixels.getPixelWidth(20)),
          // height: FetchPixels.getPixelHeight(200),
          decoration:
          BoxDecoration(color: R.colors.transparent),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  // margin: EdgeInsets.only(top: FetchPixels.getPixelHeight(100),right: FetchPixels.getPixelWidth(200)),
                  height: FetchPixels.getPixelHeight(350),

                  width: FetchPixels.getPixelWidth(200),
                  decoration: BoxDecoration(
                      color: R.colors.cirContainer,
                      shape: BoxShape.circle),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "3.",
                    style: R.textStyle.semiBoldLato().copyWith(
                        fontSize: FetchPixels.getPixelWidth(80),
                        color: R.colors.extraBoldColor),
                  ),
                  getHorSpace(40),
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: FetchPixels.getPixelHeight(30)),
                    child: Text(
                      R.strings.MitnureinemKlick,
                      style: R.textStyle
                          .semiBoldLato()
                          .copyWith(
                          fontSize:
                          FetchPixels.getPixelWidth(12),
                          color: R.colors.extraBoldColor),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(top: 100.0),
                  child: getAssetImage(R.images.jobImage4,
                      height: FetchPixels.getPixelHeight(250),
                      width: FetchPixels.getPixelWidth(150)),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
