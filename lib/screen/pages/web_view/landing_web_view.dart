import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../base/resizer/fetch_pixels.dart';
import '../../../base/widget_utils.dart';
import '../../../resources/resources.dart';
import '../../../widgets/custom_button.dart';
import '../../../widgets/my_button.dart';
import '../../clippers/clippers_class.dart';
import '../../provider/landing_page_provider.dart';
class LandingWebView extends StatelessWidget {
  const LandingWebView({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            ClipPath(
              clipper: WaveClipper(),
              child: Container(
                height: FetchPixels.getPixelHeight(350),
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      R.colors.containerG1,
                      R.colors.containerG2,
                    ]),
                    color: R.colors.blackColor),
                child: Consumer<LandingPageProvider>(
                  builder: (context, provider, child) {
                    return Row(
                      children: [
                        getHorSpace(50),
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisAlignment:
                            MainAxisAlignment.center,
                            children: [
                              Text(
                                R.strings.deineJobWebsite,
                                style: R.textStyle
                                    .semiBoldLato()
                                    .copyWith(
                                    fontSize: FetchPixels
                                        .getPixelWidth(10),
                                    color:
                                    R.colors.boldTextColor),
                              ),
                              getVerSpace(20),
                              provider.scrolledDownList == false
                                  ? Center(
                                child: MyButton(
                                  width: FetchPixels
                                      .getPixelWidth(100),
                                  onTap: () {},
                                  buttonText: R.strings
                                      .kostenlosRegistrieren,
                                ),
                              )
                                  : SizedBox(),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(
                            height: FetchPixels.getPixelHeight(250),
                            width: FetchPixels.getPixelWidth(250),
                            decoration: BoxDecoration(
                                color: R.colors.whiteColor,
                                shape: BoxShape.circle,
                                image: getDecorationAssetImage(
                                    context, R.images.jobImage1)),
                          ),
                        ),
                        getHorSpace(100),
                      ],
                    );
                  },

                ),
              ),
            ),
            Container(
              height: FetchPixels.getPixelHeight(350),
              decoration:
              BoxDecoration(color: R.colors.transparent),
              child: Column(
                children: [
                  Consumer<LandingPageProvider>(
                    builder: (context, provider, child) {
                      return Row(
                        mainAxisAlignment:
                        MainAxisAlignment.center,
                        children: [
                          customButton(
                              onClick: () {
                                provider.toogleIndex = 0;
                                provider.update();
                              },
                              topLeftRadius: 15,
                              bottomLeftRadius: 15,
                              bgColor: provider.toogleIndex == 0
                                  ? R.colors.textColorGreen
                                  : Colors.white,
                              text: R.strings.arbeitnehmer,
                              textColor: provider.toogleIndex ==
                                  0
                                  ? Colors.white
                                  : R.colors.textColorGreen),
                          customButton(
                              onClick: () {
                                provider.toogleIndex = 1;
                                provider.update();
                              },
                              bgColor: provider.toogleIndex == 1
                                  ? R.colors.textColorGreen
                                  : Colors.white,
                              text: R.strings.arbeitgeber,
                              textColor: provider.toogleIndex ==
                                  1
                                  ? Colors.white
                                  : R.colors.textColorGreen),
                          customButton(
                              onClick: () {
                                provider.toogleIndex = 2;
                                provider.update();
                              },
                              topRightRadius: 15,
                              bottomRightRadius: 15,
                              bgColor: provider.toogleIndex == 2
                                  ? R.colors.textColorGreen
                                  : Colors.white,
                              text: R.strings.temporarburo,
                              textColor:
                              provider.toogleIndex == 2
                                  ? Colors.white
                                  : R.colors.textColorGreen)
                        ],
                      );
                    },
                  ),
                  getVerSpace(10),
                  Text(
                    R.strings.Dreieinfache,
                    style: R.textStyle.semiBoldLato().copyWith(
                        fontSize: FetchPixels.getPixelWidth(8),
                        color: R.colors.boldTextColor),
                  ),
                  getVerSpace(10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: FetchPixels.getPixelHeight(150),
                        width: FetchPixels.getPixelWidth(102),
                        child: Stack(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                height:
                                FetchPixels.getPixelHeight(
                                    105),
                                width:
                                FetchPixels.getPixelWidth(
                                    105),
                                decoration: BoxDecoration(
                                    color:
                                    R.colors.cirContainer,
                                    shape: BoxShape.circle),
                              ),
                            ),
                            Center(
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.end,
                                crossAxisAlignment:
                                CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "1.",
                                    style: R.textStyle
                                        .semiBoldLato()
                                        .copyWith(
                                        fontSize: FetchPixels
                                            .getPixelWidth(
                                            16),
                                        color: R.colors
                                            .boldTextColor),
                                  ),
                                  getHorSpace(4),
                                  Padding(
                                    padding:
                                    const EdgeInsets.only(
                                        bottom: 15),
                                    child: Text(
                                      R.strings
                                          .ErstellendeinLebenslauf,
                                      style: R.textStyle
                                          .semiBoldLato()
                                          .copyWith(
                                          fontSize: FetchPixels
                                              .getPixelWidth(
                                              4),
                                          color: R.colors
                                              .boldTextColor),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      getAssetImage(R.images.jobImage2,
                          height:
                          FetchPixels.getPixelHeight(200),
                          width:
                          FetchPixels.getPixelWidth(120)),
                    ],
                  ),
                ],
              ),
            ),
            getVerSpace(50),
            Container(
              height: FetchPixels.getPixelHeight(300),
              decoration: BoxDecoration(
                image: getDecorationAssetImage(context, R.images.bgImage,fit: BoxFit.fill),

                  color: R.colors.transparent),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  getAssetImage(R.images.jobImage3,
                      height: FetchPixels.getPixelHeight(150),
                      width: FetchPixels.getPixelWidth(100)),
                  SizedBox(
                    height: FetchPixels.getPixelHeight(150),
                    width: FetchPixels.getPixelWidth(102),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            height:
                            FetchPixels.getPixelHeight(
                                105),
                            width: FetchPixels.getPixelWidth(
                                105),
                            decoration: BoxDecoration(
                                color: R.colors.transparent,
                                shape: BoxShape.circle),
                          ),
                        ),
                        Center(
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.end,
                            crossAxisAlignment:
                            CrossAxisAlignment.end,
                            children: [
                              Text(
                                "2.",
                                style: R.textStyle
                                    .semiBoldLato()
                                    .copyWith(
                                    fontSize: FetchPixels
                                        .getPixelWidth(
                                        16),
                                    color: R.colors
                                        .boldTextColor),
                              ),
                              getHorSpace(4),
                              Padding(
                                padding:
                                const EdgeInsets.only(
                                    bottom: 15),
                                child: Text(
                                  R.strings
                                      .ErstellendeinLebenslauf,
                                  style: R.textStyle
                                      .semiBoldLato()
                                      .copyWith(
                                      fontSize: FetchPixels
                                          .getPixelWidth(
                                          4),
                                      color: R.colors
                                          .boldTextColor),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            getVerSpace(50),
            Container(
              height: FetchPixels.getPixelHeight(200),
              decoration:
              BoxDecoration(color: R.colors.transparent),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: FetchPixels.getPixelHeight(150),
                    width: FetchPixels.getPixelWidth(102),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            height:
                            FetchPixels.getPixelHeight(105),
                            width:
                            FetchPixels.getPixelWidth(105),
                            decoration: BoxDecoration(
                                color: R.colors.cirContainer,
                                shape: BoxShape.circle),
                          ),
                        ),
                        Center(
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.end,
                            crossAxisAlignment:
                            CrossAxisAlignment.center,
                            children: [
                              Text(
                                "3.",
                                style: R.textStyle
                                    .semiBoldLato()
                                    .copyWith(
                                    fontSize: FetchPixels
                                        .getPixelWidth(16),
                                    color: R.colors
                                        .boldTextColor),
                              ),
                              getHorSpace(4),
                              Padding(
                                padding: const EdgeInsets.only(
                                    bottom: 5),
                                child: Text(
                                  R.strings.MitnureinemKlick,
                                  style: R.textStyle
                                      .semiBoldLato()
                                      .copyWith(
                                      fontSize: FetchPixels
                                          .getPixelWidth(4),
                                      color: R.colors
                                          .boldTextColor),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  getAssetImage(R.images.jobImage4,
                      height: FetchPixels.getPixelHeight(250),
                      width: FetchPixels.getPixelWidth(150)),
                ],
              ),
            ),
          ],
        ),
        Positioned(
          height: FetchPixels.getPixelHeight(1500),
          width: FetchPixels.getPixelWidth(350),
          child: Center(
            child: getAssetImage(R.images.arrow1,
                height: FetchPixels.getPixelHeight(300),
                width: FetchPixels.getPixelWidth(100)),
          ),
        ),
        Positioned(
            height: FetchPixels.getPixelHeight(2100),
            width: FetchPixels.getPixelWidth(330),
            child: Center(
              child: getAssetImage(R.images.arrow2,
                  height: FetchPixels.getPixelHeight(250),
                  width: FetchPixels.getPixelWidth(140)),
            )),
      ],
    );
  }
}
