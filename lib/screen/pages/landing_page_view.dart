import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:task_project/base/device_util.dart';
import 'package:task_project/base/resizer/fetch_pixels.dart';
import 'package:task_project/screen/provider/landing_page_provider.dart';

import '../../resources/resources.dart';
import '../../widgets/my_button.dart';
import 'mobile_view/landing_mobile_view.dart';
import 'web_view/landing_web_view.dart';

class LandingPageView extends StatefulWidget {
  const LandingPageView({super.key});

  @override
  State<LandingPageView> createState() => _LandingPageViewState();
}

class _LandingPageViewState extends State<LandingPageView> {
  @override
  void initState() {
    var pro = Provider.of<LandingPageProvider>(context, listen: false);
    pro.listViewController.addListener(pro.listViewScrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FetchPixels(context);
    return Scaffold(
      bottomNavigationBar: DeviceUtil.isTablet
          ? null
          : Container(
              padding: EdgeInsets.symmetric(
                  horizontal: FetchPixels.getPixelWidth(20),
                  vertical: FetchPixels.getPixelHeight(10)),
              height: FetchPixels.getPixelHeight(60),
              width: FetchPixels.width,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: R.colors.blackColor.withOpacity(0.3),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  color: R.colors.whiteColor,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(
                      FetchPixels.getPixelWidth(5),
                    ),
                    topLeft: Radius.circular(
                      FetchPixels.getPixelWidth(5),
                    ),
                  )),
              child: Align(
                alignment: Alignment.centerRight,
                child: MyButton(
                  // width: FetchPixels.getPixelWidth(100),
                  onTap: () {},
                  buttonText: R.strings.kostenlosRegistrieren,
                ),
              ),
            ),
      appBar: AppBar(
        backgroundColor: R.colors.transparent,
        elevation: 0.0,
        flexibleSpace: Consumer<LandingPageProvider>(
          builder: (context, provider, child) {
            return Container(
              padding: EdgeInsets.symmetric(
                  horizontal: FetchPixels.getPixelWidth(7)),
              height: FetchPixels.getPixelHeight(50),
              width: FetchPixels.width,
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: R.colors.blackColor.withOpacity(0.3),
                        blurRadius: 5,
                        spreadRadius: 2)
                  ],
                  color: R.colors.whiteColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(
                      FetchPixels.getPixelWidth(DeviceUtil.isTablet ? 5 : 10),
                    ),
                    bottomRight: Radius.circular(
                      FetchPixels.getPixelWidth(DeviceUtil.isTablet ? 5 : 10),
                    ),
                  )),
              child: Align(
                  alignment: Alignment.centerRight,
                  child: provider.scrolledDownList && DeviceUtil.isTablet
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: MyButton(
                            width: FetchPixels.getPixelWidth(100),
                            onTap: () {},
                            buttonText: R.strings.kostenlosRegistrieren,
                          ),
                        )
                      : Text(
                          R.strings.login,
                          style: R.textStyle.mediumLato().copyWith(
                              fontSize: FetchPixels.getPixelWidth(
                                  DeviceUtil.isTablet ? 4 : 10),
                              color: R.colors.textColorGreen),
                        )),
            );
          },
        ),
      ),
      body: Consumer<LandingPageProvider>(
        builder: (context, provider, child) {
          return ListView(
            controller: provider.listViewController,
            padding: EdgeInsets.zero,
            children: [
              DeviceUtil.isTablet
                  ? LandingWebView()
                  : LandingMobileView(),
            ],
          );
        },
      ),
    );
  }
}
